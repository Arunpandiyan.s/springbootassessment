package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.dao.PortDAO;
import com.example.demo.mailservice.EmailSenderService;
import com.example.demo.model.Port;
import com.example.demo.portcheck.PortCheck;


@Controller
public class PortController {
	
	@Autowired
	@Qualifier("emailSenderService")
	private EmailSenderService senderService;
	
	@Autowired
	@Qualifier("portCheck")
	private PortCheck portCheck;

	
	@Autowired
	private PortDAO portDAO;
	
	@RequestMapping("/")
		public String Home() {
		return "index.jsp";
	}
	
	@RequestMapping("/add")
	@ResponseBody
	public String add(Port port) {
		portDAO.save(port);
		return "Add";
	}
	@RequestMapping("/get")
	@ResponseBody
	public Optional<Port> get(int id) {
		return portDAO.findById(id);
	}
	@RequestMapping("/getAll")
	@ResponseBody
	public List<Port> getAll() {
		return portDAO.findAll();
	}
	
	@RequestMapping("/status/{port}")
	@ResponseBody
	public String getStatusByPort(@PathVariable("port") int port) {
		int portnumber=port;
		String s=String.valueOf(portnumber);
		String str=portCheck.isRemotePortInUse("localhost",port);
		String s1="Port "+s+":"+str;
		senderService.sendEmail("arunpandiyan25ya@gmail.com","Port status" , s1);
		return str;	
	}
	
	@RequestMapping("/sts")
	@ResponseBody
	public String getBYPort(int port) {
		int portnumber=port;
		String s=String.valueOf(portnumber);
		String str=portCheck.isRemotePortInUse("localhost",port);
		String s1="Port "+s+":"+str;
		senderService.sendEmail("arunpandiyan25ya@gmail.com","Port status" , s1);
		return str;	
	}
	
}
