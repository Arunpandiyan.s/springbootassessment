package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Port;


public interface PortDAO extends JpaRepository<Port,Integer>{
	

}
