package com.example.demo.mailservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
@Service
public class EmailSenderService {
	
	@Autowired
	private JavaMailSender mailSender;
	
    public void sendEmail(String toEmail,String subject,String body) {
		
		SimpleMailMessage message=new SimpleMailMessage();
		
		message.setFrom("arunpandiyan25ya@gmailcom");
		message.setTo(toEmail);
		message.setText(body);
		message.setSubject(subject);
		
		mailSender.send(message);
		
		System.out.println("Mail sent successfuly.....!");
	}

}
