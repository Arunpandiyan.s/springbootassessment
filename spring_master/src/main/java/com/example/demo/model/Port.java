package com.example.demo.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity

public class Port{
	
	@Id
	@Column(name="id")
	private int id;
	@Column(name="portnumber")
	private int portnumber;
	
	public Port() {}

	public Port(int id, int portnumber) {
		super();
		this.id = id;
		this.portnumber = portnumber;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPortnumber() {
		return portnumber;
	}

	public void setPortnumber(int portnumber) {
		this.portnumber = portnumber;
	}
	
}