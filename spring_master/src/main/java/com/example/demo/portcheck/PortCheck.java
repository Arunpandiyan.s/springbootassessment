package com.example.demo.portcheck;

import java.io.IOException;
import java.net.Socket;

import org.springframework.stereotype.Component;

@Component
public class PortCheck {

	public static String isRemotePortInUse(String hostName, int portNumber) {
	    try {
	        // Socket try to open a REMOTE port
	        new Socket(hostName, portNumber).close();
	      
	      return "ACTIVE NOW......!";
	    } catch(IOException e) {
	       	return "OOPS! NOT ACTIVE..";
	    }
	}

}
