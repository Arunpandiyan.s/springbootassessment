package com.codeboard.springboot.dao;

import java.util.List;

import com.codeboard.springboot.entity.Port;


public interface PortDAO {
	
	List<Port> findAll();
	Port findById(int id);

}
