package com.codeboard.springboot.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.codeboard.springboot.entity.Port;

import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;

public class PortDAOImpl implements PortDAO{
	

	private EntityManager entityManager;
	
	
	@Autowired
	public PortDAOImpl(EntityManager theentityManager) {
		entityManager = theentityManager;
	}

	@Override
	public List<Port> findAll() {
		TypedQuery<Port> theQuery=entityManager.createQuery("FROM port",Port.class);
	    List<Port> ports=theQuery.getResultList();
		return ports;
	}

	@Override
	public Port findById(int id) {
		Port thePort=entityManager.find(Port.class,id);
		return thePort;
	}

}
