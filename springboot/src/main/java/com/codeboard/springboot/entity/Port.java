package com.codeboard.springboot.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="port")
public class Port {
	
	//define fields
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="port")
	private int port;
	
	//constructors
	public Port() {}

	public Port(int port) {
		this.port = port;
	}

	
	
	
	//Getters and setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	//toString
	@Override
	public String toString() {
		return "Port [id=" + id + ", port=" + port + "]";
	}
	
	
	
	
	

}
