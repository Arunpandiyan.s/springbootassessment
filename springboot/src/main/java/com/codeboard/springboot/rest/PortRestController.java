package com.codeboard.springboot.rest;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codeboard.springboot.dao.PortDAO;
import com.codeboard.springboot.entity.Port;


@RestController
@RequestMapping("/api")
public class PortRestController {
	
	private PortDAO portDAO;
	private List<Port> thePort;
	
	public PortRestController(PortDAO thePortDAO) {
		portDAO=thePortDAO;
	}
	
	
	@GetMapping("/ports")
	public List<Port> findAll(){
		return portDAO.findAll();
	}
	
	@GetMapping("/ports/{portId}")
	public Port getEmployee(@PathVariable int portId) {
		Port thePort= portDAO.findById(portId);
		
		return thePort;
	}

}
